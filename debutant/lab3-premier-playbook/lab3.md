# Lab 3
## Objectif 

Configurer des groupes de serveurs à l’aide d’un playbook 
Utilisez l’exemple de la page suivante 
Objectif 
Creer un playbook appelé lab3.yaml qui va exécuter ces tâches :

- Configurez SELINUX en mode permissif sur tous vos hôtes
- Installez et activer HTTPD seulement sur vos hôtes du groupe Web
- Copiez un fichier motd indiquant « Bienvenue sur mon serveur! » sur tous vos hôtes (/etc/motd)
- Copiez un fichier index.html « Hello World » sur vos hôtes du groupe Web dans /var/www/html 
- Commettre votre playbook dans le serveur git 
